package com.sunil.myvehicle;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
/**
 * Created by 12071134 on 16/01/2019.
 */


public class Email {
    private Context context;

    private String[] recipients;
    private String[] carbonCopies = {};
    private String subject;
    private String message;

    public Email(Context context) {
        this.context = context;
    }

    public void send() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
        emailIntent.putExtra(Intent.EXTRA_CC, carbonCopies);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        emailIntent.setType("message/rfc822");
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Assignment2", "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }

    }

    public void setRecipients(String[] recipients) {
        this.recipients = recipients;
    }

    public void setCarbonCopies(String[] carbonCopies) {
        this.carbonCopies = carbonCopies;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
