package com.sunil.myvehicle;

/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class User {


    /**
     * Declare the variable for the User Table
     */
        private int id;
        private String loginUsername;
        private String loginPassword;
        private String loginRepeatPassword;

        public static final String userTABLE = "USER";

        //Column Name Define for Vehicle
        public static String ID = "ID";
        public static String LoginUsernameColumn = "loginUsername";
        public static String LoginPasswordColumn = "loginPassword";
        public static String LoginRepeatPasswordColumn = "loginRepeatPassword";


        public User(){}
        // Create table SQL query

        public static final String CREATE_USER_TABLE =
                "CREATE TABLE  " + userTABLE + "(" +
                        ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        LoginUsernameColumn + " TEXT ," +
                        LoginPasswordColumn + " TEXT," +
                        LoginRepeatPasswordColumn + " TEXT" +
                        ")";


        public User(String loginUsername, String loginPassword, String loginRepeatPassword) {
            this.loginUsername = loginUsername;
            this.loginPassword = loginPassword;
            this.loginRepeatPassword = loginRepeatPassword;

        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLoginUsername() {
            return loginUsername;
        }

        public void setLoginUsername(String loginUsername) {
            this.loginUsername = loginUsername;
        }

        public String getLoginPassword() {
            return loginPassword;
        }

        public void setLoginPassword(String loginPassword) {
            this.loginPassword = loginPassword;
        }

        public String getLoginRepeatPassword() {
            return loginRepeatPassword;
        }

        public void setLoginRepeatPassword(String loginRepeatPassword) {
            this.loginRepeatPassword = loginRepeatPassword;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", loginUsername='" + loginUsername + '\'' +
                    ", loginPassword='" + loginPassword + '\'' +
                    ". loginRepeatPassword = '" + loginRepeatPassword + '\''+
                    '}';
        }
}
