package com.sunil.myvehicle;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class SubmitFragment extends DialogFragment {
    public static boolean emailSent = false;

    static SubmitFragment newInstance(String title, String message) {
        SubmitFragment fragment = new SubmitFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        return new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                new DoBackgroundTask(getActivity()).execute();
                                Log.e("Assignment2", "send email "+emailSent);
                            }
                        })
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                Toast.makeText(getActivity(), "Cancelled.", Toast.LENGTH_SHORT).show();

                            }
                        }).create();
    }

    private class DoBackgroundTask extends AsyncTask<Void, Integer, Long> {
        private ProgressDialog dialog;
        private Context c;

        public DoBackgroundTask(Context c) {
            this.c = c;
            dialog = new ProgressDialog(c);
        }
        @Override
        protected void onPreExecute() {
            dialog.setMessage("Sending Email, please wait.");
            dialog.show();
        }
        protected Long doInBackground(Void... params) {
            DatabaseHelper db = new DatabaseHelper(getActivity());
            ArrayList<VehicleLog> vehicleLogArrayList;
            for (VehicleLog s : MainActivity.vehicleLog_List) {
                db.insertVehicleLog(s);
            }
            vehicleLogArrayList = (ArrayList<VehicleLog>) MainActivity.vehicleLog_List;


            User user = db.getUser();
            String userslog = user.getLoginUsername();
            StringBuilder message = new StringBuilder(user.getLoginUsername() + ",\n\n");
            for (VehicleLog shedLogs : vehicleLogArrayList) {
                message.append(shedLogs.format()).append("\n\n");
            }

            Email email = new Email(getActivity());
            email.setMessage(message.toString());
            String[] to = {"deryck.sunil@gmail.com"};
            email.setRecipients(to);
            email.setSubject("New Logger data");
            email.send();
            MainActivity.vehicleLog_List.clear();
            db.delete();

            db.close();
            return null;
        }

        protected void onPostExecute(Long result) {

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
//            AlertToast.message(c, "message sent.");
        }
    }
}