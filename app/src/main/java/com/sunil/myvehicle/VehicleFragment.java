package com.sunil.myvehicle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class VehicleFragment extends Fragment {
    private String[] pageNames = {"Car", "5T Truck", "10T Truck", "Tipper", "Articulated"};
    Integer vehicleIndex;
    TextView txtVehicleName,txtStartTime,txtFirstBreak,txtSecondBreak, txtEndTime;
    Button btnStartTime, btnFirstBreak, btnSecondBreak, btnEndTime,btnShowLogEntries, btnSaveLogEntry;
    Button btnNext,btnPrevious,btnHome;
    EditText txtDriverName, txtRego;
    DatabaseHelper myDB;
    TrackGPS gps;
    String longitude = null;
    String latitude = null;

    /**
     *   Required empty public constructor
     */
    public VehicleFragment() {
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.vehicle_fragment, container, false);

        btnStartTime = (Button) view.findViewById(R.id.btnStartTime);
        txtStartTime = (TextView) view.findViewById(R.id.txtStartTime);
        btnFirstBreak = (Button) view.findViewById(R.id.btnFirstBreak);
        txtFirstBreak = (TextView) view.findViewById(R.id.txtFirstBreak);
        btnSecondBreak = (Button) view.findViewById(R.id.btnSecondBreak);
        txtSecondBreak = (TextView) view.findViewById(R.id.txtSecondBreak);
        btnEndTime = (Button) view.findViewById(R.id.btnEndTime);
        txtEndTime = (TextView) view.findViewById(R.id.txtEndTime);
        btnSaveLogEntry =  (Button) view.findViewById(R.id.btnSaveLogEntry);
        txtDriverName = (EditText) view.findViewById(R.id.txtDriverName);
        txtRego = (EditText) view.findViewById(R.id.txtRego);
        btnPrevious = (Button) view.findViewById(R.id.btnPrevious);
        btnNext = (Button) view.findViewById(R.id.btnNext);
        btnHome = (Button) view.findViewById(R.id.btnHome);

        btnSaveLogEntry = (Button) view.findViewById(R.id.btnSaveLogEntry);
        btnShowLogEntries = (Button) view.findViewById(R.id.btnShowLogEntries);

        vehicleIndex = getArguments().getInt("vehicle");        //Get the Argument from MainActivity as its pageIndex
        txtVehicleName = (TextView) view.findViewById(R.id.txtVehicleName);
        txtVehicleName.setText(pageNames[vehicleIndex]);
        onStart();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        gps = new TrackGPS(this.getActivity());
        if (gps.canGetLocation()) {
            longitude = String.valueOf(gps.getLongitude());
            latitude = String.valueOf(gps.getLatitude());
        }
    }

    /**
     * clear form data
     */
    public void clear() {

        //myDB.addData(Driver,Rego,Start,First,Second,End);
        txtDriverName.setText("");
        txtRego.setText("");
        txtStartTime.setText("");
        txtFirstBreak.setText("");
        txtSecondBreak.setText("");
        txtEndTime.setText("");
    }


    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /**
         * Displays the time along with text while
         * clicking the button
         * and will invisible
         */

        onStart();
        btnStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String strDate  = sdf.format(c.getTime());
                txtStartTime.setText("Start of Time: " + strDate + " "+"Longtitude: " + longitude + " "+"Latitiude: " +latitude);
                btnFirstBreak.setVisibility(View.VISIBLE);
                btnStartTime.setVisibility(View.INVISIBLE);
            }
        });

        /**
         * Displays the time along with text while
         * clicking the button
         * and will invisible
         */
        btnFirstBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String strDate  = sdf.format(c.getTime());
                txtFirstBreak.setText("1st Break: " + strDate + " "+"Longtitude: " + longitude + " "+"Latitiude: " +latitude);
                btnSecondBreak.setVisibility(View.VISIBLE);
                btnFirstBreak.setVisibility(View.INVISIBLE);
            }
        });

        /**
         * Displays the time along with text while
         * clicking the button
         * and will invisible
         */

        btnSecondBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String strDate  = sdf.format(c.getTime());
                txtSecondBreak.setText("2nd Break: " + strDate + " "+"Longtitude: " + longitude + " "+"Latitiude: " +latitude);
                btnEndTime.setVisibility(View.VISIBLE);
                btnSecondBreak.setVisibility(View.INVISIBLE);
            }
        });


        /**
         * Displays the time along with text while
         * clicking the button
         * and will invisible
         */
        btnEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String strDate  = sdf.format(c.getTime());
                txtEndTime.setText("End of Time: "+ strDate + " "+"Longtitude: " + longitude + " "+"Latitiude: " +latitude);
                btnEndTime.setVisibility(View.INVISIBLE);
                btnSecondBreak.setVisibility(View.INVISIBLE);
            }
        });

        //It will saves the Log entry of the vehicle

        btnSaveLogEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Driver = txtDriverName.getText().toString();
                String Rego = txtRego.getText().toString();
                String Start = txtStartTime.getText().toString();
                String First = txtFirstBreak.getText().toString();
                String Second = txtSecondBreak.getText().toString();
                String End = txtEndTime.getText().toString();



                if(!emptyValidation()){

                    try{

                        VehicleLog vehicleLog = new VehicleLog(Driver,Rego,Start,First,Second,End);
                        vehicleLog.setVehicleList(vehicleIndex);
                        MainActivity.vehicleLog_List.add(vehicleLog);
                        Toast.makeText(getContext(), "Entry saved.", Toast.LENGTH_LONG).show();
                        clear();
                        btnStartTime.setVisibility(v.VISIBLE);

                    }
                    catch (Exception e){
                        Toast.makeText(getContext(), "Entry not saved as not all data entered. Complete all entries and try again.", Toast.LENGTH_LONG).show();

                    }
                }
                else{
                    Toast.makeText(getContext(), "Entry not saved as not all data entered. Complete all entries and try again.", Toast.LENGTH_LONG).show();
                }

            }
        });


        //Shows all the entries of the Particular Selected Index as vehicle types
        //In ListView
        btnShowLogEntries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment listFragments = new ListFragments();
                Bundle args = new Bundle();
                args.putInt("vehicle", vehicleIndex);
                listFragments.setArguments(args);
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.mainActivity, listFragments,"listFragments");
                transaction.addToBackStack("listFragments");
                transaction.commit();
            }
        });
        timeButton();
    }

    //Condition applys for the button
    //If all the condition are save in ArrayList
    //Which will make the buttton as Invisible

    private void timeButton(){
        btnFirstBreak = getActivity().findViewById(R.id.btnFirstBreak);
        btnFirstBreak.setVisibility(View.INVISIBLE);
        btnSecondBreak = getActivity().findViewById(R.id.btnSecondBreak);
        btnSecondBreak.setVisibility(View.INVISIBLE);
        btnEndTime = getActivity().findViewById(R.id.btnEndTime);
        btnEndTime.setVisibility(View.INVISIBLE);

    }


    //Check Whether the edittext is empty or not

    public boolean emptyValidation(){
        if (TextUtils.isEmpty(txtDriverName.getText().toString()) ||
                TextUtils.isEmpty(txtRego.getText().toString()) ||
                TextUtils.isEmpty(txtStartTime.getText().toString()) ||
                TextUtils.isEmpty(txtFirstBreak.getText().toString()) ||
                TextUtils.isEmpty(txtSecondBreak.getText().toString()) ||
                TextUtils.isEmpty(txtEndTime.getText().toString()))
            return true;
        else
            return false;

    }
}
