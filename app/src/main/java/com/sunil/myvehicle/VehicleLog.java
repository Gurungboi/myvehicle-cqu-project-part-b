package com.sunil.myvehicle;


/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class VehicleLog {
    //Table Name
    public static final String vehicleTable = "Vehicles";

    //Column Name Define for Vehicle
    public static final String idColumn = "id";
    public static final String driverColumn = "driver";
    public static final String regonoColumn = "regono";
    public static final String startColumn = "start";
    public static final String firstColumn = "first";
    public static final String secondColumn = "second";
    public static final String endColumn = "endTime";
    public static final String vehicleListColumn = "vehicleList";


    //public static String CREATE_VEHICLE_TABLE = "CREATE TABLE " + vehicleTable + " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
    //        " driver TEXT, regono TEXT, start TEXT, first TEXT, second TEXT, endTime TEXT)";

    // Create table SQL query
    public static final String CREATE_VEHICLE_TABLE =
            "CREATE TABLE " + vehicleTable + "("
                    + idColumn + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + driverColumn + " TEXT,"
                    + regonoColumn + " TEXT,"
                    + startColumn + " TEXT,"
                    + firstColumn + " TEXT,"
                    + secondColumn + " TEXT,"
                    + endColumn + " TEXT,"
                    + vehicleListColumn + " INTEGER"
                    + ")";

    public VehicleLog(){}
    private String DriverName,RegoNo,StartTime,FirstBreak,SecondBreak,EndTime;
    private int id;
    private int vehicleList;

    public int getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(int vehicleList) {
        this.vehicleList = vehicleList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public  String getDriverName(){
        return DriverName;
    }

    public void setDriverName(String DriverName){
        this.DriverName = DriverName;
    }

    public String getRegoNo(){
        return RegoNo;
    }

    public void setRegoNo(String RegoNo){
        this.RegoNo = RegoNo;
    }

    public String getStartTime(){
        return StartTime;
    }

    public void setStartTime(String StartTime){
        this.StartTime = StartTime;
    }

    public String getFirstBreak(){
        return FirstBreak;
    }

    public void setFirstBreak(String FirstBreak){
        this.FirstBreak = FirstBreak;
    }

    public String getSecondBreak(){
        return SecondBreak;
    }

    public void setSecondBreak(String SecondBreak){
        this.SecondBreak = SecondBreak;
    }

    public String getEndTime(){
        return EndTime;
    }

    public void setEndTime(String EndTime){
        this.EndTime = EndTime;
    }

    public String format() {
        return DriverName + " " + RegoNo + " " + StartTime + " " + FirstBreak + " " + SecondBreak + " " + EndTime + " " ;
    }


    public VehicleLog(String DriverName,String RegoNo,String StartTime,String FirstBreak,String SecondBreak, String EndTime){
        this.DriverName = DriverName;
        this.RegoNo = RegoNo;
        this.StartTime = StartTime;
        this.FirstBreak = FirstBreak;
        this.SecondBreak = SecondBreak;
        this.EndTime = EndTime;
    }


    @Override
    public String toString() {
        return "VehicleLog {" +
                "DriverName='" + DriverName + '\'' +
                ", RegoNo=" + RegoNo +
                ", StartTime=" + StartTime +
                ", FirstBreak=" + FirstBreak +
                ", SecondBreak=" + SecondBreak +
                ", EndTime='" + EndTime + '\'' +
                '}';
    }
}
