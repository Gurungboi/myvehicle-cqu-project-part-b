package com.sunil.myvehicle;

import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class HomeFragment extends Fragment{

    /**
     * It will required to create the homefragment
     * due to constructor
     */
    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home_fragment,container,false);
        return view;
    }
}
