package com.sunil.myvehicle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class ListFragments extends ListFragment {

   //Declare the variable

    int vehicleIndex;
    DatabaseHelper db;
    ArrayList<VehicleLog> vehicleLogArrayList;
    Button btnReturn;
    String[] pageNames = {"Car", "5T Truck", "10T Truck", "Tipper", "Articulated"};

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        vehicleIndex = getArguments().getInt("vehicle");
        btnReturn = (Button) view.findViewById(R.id.btnReturn);

        db = new DatabaseHelper(getActivity());
        if (MainActivity.filterdList(vehicleIndex).isEmpty()) {
            vehicleLogArrayList = db.getVehicleLogEntry(vehicleIndex);
            db.close();
        } else {
            vehicleLogArrayList = MainActivity.filterdList(vehicleIndex);
        }




        //Condition whether the vehicleLogArrayList is null or not
        //If it is null, it will be blank while
        //If it is not, it will display all the list of the index
        if(vehicleLogArrayList !=null){
            String[] vehiclesArray = new String[vehicleLogArrayList.size()];
            for (int i = 0; i < vehicleLogArrayList.size(); i++) {
                vehiclesArray[i] = vehicleLogArrayList.get(i).format();
            }
            setListAdapter(new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, vehiclesArray));
        }
        else{
        }

        //Return to the previous page

        btnReturn.setText("Return To " + pageNames[vehicleIndex]);
        return view;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
