package com.sunil.myvehicle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import static com.sunil.myvehicle.VehicleLog.vehicleTable;

/**
 * Created by Sunil Gurung on 14/12/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    /**
     * Declare the variabl
     * version
     * name
     * log
     */

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "MyVehicleDB";
    private static final String LOG = "MyVehicle";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Create the Database
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(VehicleLog.CREATE_VEHICLE_TABLE);
        db.execSQL(User.CREATE_USER_TABLE);
    }

    /**
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // you can implement here migration process
        db.execSQL("DROP TABLE IF EXISTS " + VehicleLog.vehicleTable);
        db.execSQL("DROP TABLE IF EXISTS " + User.userTABLE);

        this.onCreate(db);
    }

    /**
     * Insert the Data into the vehicelog
     * @param vehicleLog
     * @return
     */


    public VehicleLog insertVehicleLog(VehicleLog vehicleLog){
        SQLiteDatabase db = this.getWritableDatabase();

        try{
            ContentValues values = new ContentValues();
            values.put(vehicleLog.driverColumn, vehicleLog.getDriverName());
            values.put(vehicleLog.regonoColumn, vehicleLog.getRegoNo());
            values.put(vehicleLog.startColumn, vehicleLog.getStartTime());
            values.put(vehicleLog.firstColumn, vehicleLog.getFirstBreak());
            values.put(vehicleLog.secondColumn, vehicleLog.getSecondBreak());
            values.put(vehicleLog.endColumn, vehicleLog.getEndTime());
            values.put(vehicleLog.vehicleListColumn, vehicleLog.getVehicleList());

            Long id = db.insert(VehicleLog.vehicleTable, null, values);
            vehicleLog.setId(id.intValue());
            return vehicleLog;

        }catch (SQLException e) {
            db.close();
        }

        return null ;
    }


    /**
     *
     * @param vehicleNo
     * @return
     */

    public ArrayList<VehicleLog> getVehicleLogEntry(int vehicleNo) {
        ArrayList<VehicleLog> vehicleLogArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor c = db.query(VehicleLog.vehicleTable,
                    null,
                    VehicleLog.vehicleListColumn + "=?",
                    new String[]{String.valueOf(vehicleNo)},
                    null, null, null, null);

            // Loops all through the vehicleLog
            // Which will add towards the list
            //
            if (c.moveToFirst()) {
                do {
                    VehicleLog vehicleLog = new VehicleLog();
                    vehicleLog.setId(c.getInt(c.getColumnIndex(VehicleLog.idColumn)));
                    vehicleLog.setDriverName(c.getString(c.getColumnIndex(VehicleLog.driverColumn)));
                    vehicleLog.setRegoNo(c.getString(c.getColumnIndex(VehicleLog.regonoColumn)));
                    vehicleLog.setStartTime(c.getString(c.getColumnIndex(VehicleLog.startColumn)));
                    vehicleLog.setFirstBreak(c.getString(c.getColumnIndex(VehicleLog.firstColumn)));
                    vehicleLog.setSecondBreak(c.getString(c.getColumnIndex(VehicleLog.secondColumn)));
                    vehicleLog.setEndTime(c.getString(c.getColumnIndex(VehicleLog.endColumn)));
                    vehicleLog.setVehicleList(c.getInt(c.getColumnIndex(VehicleLog.vehicleListColumn)));
                    vehicleLogArrayList.add(vehicleLog);
                } while (c.moveToNext());
            }
            c.close();
            return vehicleLogArrayList;
        } catch (Exception $e) {
            Log.e("Query", $e.getMessage());
        } finally {
            db.close();         //Close the DB
        }

        return null;
    }



    public ArrayList<VehicleLog> getAllVehicleLog() {
        ArrayList<VehicleLog> vehicleLogArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + VehicleLog.vehicleTable;
        SQLiteDatabase db = this.getReadableDatabase();
        try (Cursor c = db.rawQuery(selectQuery, null)) {

            // Loops all through the vehicleLog
            // Which will add towards the list
            //
            if (c.moveToFirst()) {
                do {
                    VehicleLog vehicleLog = new VehicleLog();
                    vehicleLog.setId(c.getInt(c.getColumnIndex(VehicleLog.idColumn)));
                    vehicleLog.setDriverName(c.getString(c.getColumnIndex(VehicleLog.driverColumn)));
                    vehicleLog.setRegoNo(c.getString(c.getColumnIndex(VehicleLog.regonoColumn)));
                    vehicleLog.setStartTime(c.getString(c.getColumnIndex(VehicleLog.startColumn)));
                    vehicleLog.setFirstBreak(c.getString(c.getColumnIndex(VehicleLog.firstColumn)));
                    vehicleLog.setSecondBreak(c.getString(c.getColumnIndex(VehicleLog.secondColumn)));
                    vehicleLog.setEndTime(c.getString(c.getColumnIndex(VehicleLog.endColumn)));
                    vehicleLog.setVehicleList(c.getInt(c.getColumnIndex(VehicleLog.vehicleListColumn)));
                    vehicleLogArrayList.add(vehicleLog);
                } while (c.moveToNext());
            }
        }

        return vehicleLogArrayList;
    }


    /**
     * Delete the Detail of Vehicle Table
     * @return
     */
    public boolean delete() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(VehicleLog.vehicleTable, null, null) > 0;
    }

    /**
     * Insert into User Table
     * @param user
     * @return
     */
    public User createUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(User.LoginPasswordColumn, user.getLoginUsername());
            contentValues.put(User.LoginPasswordColumn, user.getLoginPassword());
            contentValues.put(User.LoginRepeatPasswordColumn, user.getLoginRepeatPassword());
            long id = db.insert(User.userTABLE, null, contentValues);
            user.setId((int) id);
            return user;
        } catch (SQLException e) {
            db.close();
        } finally {
            db.close();
        }
        return null;
    }


    /**
     * get user information
     * @return User
     */
    private static final String[] COLUMNS = {User.LoginUsernameColumn,User.LoginPasswordColumn,User.LoginRepeatPasswordColumn};



    public User getUser() {
        Cursor c;
   try (SQLiteDatabase db = getReadableDatabase()) {
            c = db.query(User.userTABLE,
                    null,
                    null,
                    null, null, null, "ID DESC", "1");
            if (c != null) {
                c.moveToFirst();
                return new User(
                        c.getString(1),
                        c.getString(2),
                        c.getString(3)
                );

            }

        } catch (Exception e) {
            Log.e("Assignment2", e.getMessage());
        }
        close();
        return null;

    }
}
